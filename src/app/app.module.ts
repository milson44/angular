import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ButtonComponent } from './button/button.component';
import { BageComponent } from './bage/bage.component';
import { RatingComponent } from './rating/rating.component';
import { IconComponent } from './icon/icon.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { IconTooltipComponent } from './icon-tooltip/icon-tooltip.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ButtonModule } from './button/button.module';
import { BageModule } from './bage/bage.module';
import { DropdownModule } from './dropdown/dropdown.module';
import { IconModule } from './icon/icon.module';
import { ProductCardModule } from './product-card/product-card.module';
import { IconTooltipModule } from './icon-tooltip/icon-tooltip.module';
import { RatingModule } from './rating/rating.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    ButtonModule,
    BageModule,
    DropdownModule,
    IconModule,
    ProductCardModule,
    IconTooltipModule,
    RatingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
